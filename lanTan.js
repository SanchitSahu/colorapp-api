var i18n = require('i18n');

i18n.configure({
  // setup some locales - other locales default to en silently
  locales:['es', 'en'],

  // where to store json files - defaults to './locales' relative to modules directory
  directory: __dirname + '/locales',
  //directory: __dirname + '/locales/es.json',
  
  defaultLocale: 'en',

   // register: global,
   autoReload: true,
   preserveLegacyCase: true,
   cookie: 'locales',
   queryParameter: 'lang',
});

module.exports = function(req, res, next) {
  i18n.init(req, res);
  
  var current_locale = i18n.setLocale(req,req.headers.lang);
  var current_locale1 = i18n.getLocale(current_locale);
  //req.session.locale = req.query.lang;
  return next();
};