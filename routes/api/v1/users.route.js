// Define routes for user APIs.
const express = require('express');
const router = express.Router();

const userController = require('../../../controllers/v1/user.controller');

router.post('/login', userController.login);
router.post('/addContactUs', userController.addContactUs);
router.post('/addCustomerDetails', userController.addCustomerDetails);
router.post('/addServiceRequest', userController.addServiceRequest);
router.post('/addDealer', userController.addLocalStores);
router.post('/addCmsPages', userController.addCmsPages);

router.get('/dashboard', userController.dashboard);
router.get('/getContactUs', userController.getContactUs);
router.get('/getCustomerDetails', userController.getCustomerDetails);
router.get('/getServiceRequests', userController.getServiceRequests);
router.get('/getDealer/:id', userController.getLocalStores);
router.get('/getDealerList', userController.getAllLocalStores);
router.get('/getAllCmsPages', userController.getAllCmsPages);
router.get('/getCmsPage/:type/:language', userController.getCmsPage);
router.get('/getCmsPage/:id', userController.getCmsPage);

router.put('/changePassword', userController.changePassword);
router.put('/updateDealerDetails', userController.updateLocalStores);
router.put('/updateCmsPages', userController.updateCmsPages);

router.post('/deleteCustomerDetails', userController.deleteCustomerDetails);
router.post('/deleteContactUs', userController.deleteContactUs);
router.post('/deleteServiceRequest', userController.deleteServiceRequest);
router.post('/deleteDealer', userController.deleteLocalStores);
router.post('/deleteCmsPages', userController.deleteCmsPages);

router.get('/imageCords', userController.getImageCords);
router.get('/imageWebCords', userController.getImageWebCords);
router.post('/deleteImageCords', userController.deleteImageCords);

router.get('/CustomerDetailsCsv', userController.CustomerDetailsCsv);
router.get('/ContactUsCsv', userController.ContactUsCsv);
router.get('/ServiceRequestCsv', userController.ServiceRequestCsv);


// Export the router.
module.exports = router;