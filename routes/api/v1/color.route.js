// Define routes for color APIs.
const express = require('express');
const router = express.Router();

const colorController = require('../../../controllers/v1/color.controller');

router.post('/addColor', colorController.addColor);
router.post('/addColorShade', colorController.addColorShade);

router.get('/getColors', colorController.getColors);
router.get('/getColors/:colorId', colorController.getColors);

router.get('/getColorShades/:colorId', colorController.getColorShades);
router.get('/getColorShades/:colorShadeId/:single', colorController.getColorShades);
router.get('/getColorShades', colorController.getColorShades);

router.post('/editColor', colorController.editColors);
router.post('/editColorShade', colorController.editColorShades);

router.post('/deleteColor', colorController.deleteColor);
router.post('/deleteColorShade', colorController.deleteColorShade);

// Export the router.
module.exports = router;