const express = require('express');
const router = express.Router();

const usersV1 = require('./api/v1/users.route');
const colorsV1 = require('./api/v1/color.route');

router.use('/v1/users', usersV1);
router.use('/v1/colors', colorsV1);

module.exports = router;