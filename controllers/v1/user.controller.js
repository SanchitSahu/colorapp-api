const userService = require('../../services/v1/user.service');
const sendResponse = require('../../modules/response.helper');
var i18n = require('./../../lanTan');


exports.login = async (req, res, next) => {
    try {
        var admin = {
            username: req.body.username.toLowerCase(),
        };
        var adminSigninChck = await userService.signIn(admin);
        var getPassword = await userService.getPassword(admin);
        if (adminSigninChck != null) {
            
            var adminData = await userService.checkSignIn(adminSigninChck.username, getPassword.password, req.body.password);
            
            if (adminData) {
                return sendResponse.sendJsonResponse(req, res, 200, adminData, "0", res.__('LoginUsername'));
                
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, adminData, "1", res.__('errorLoginCredential'));
            }
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, adminData, "1", res.__('errorLoginUsername'));
        }
    }
    catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.dashboard = function (req, res, next) {
    userService.getDashboard((dashboard) => {
        if (dashboard) {
            return sendResponse.sendJsonResponse(req, res, 200, dashboard, "0", res.__('Dashboard'));
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, dashboard, "1", res.__('errorDashboard'));
        }
    })
}

exports.addContactUs = (req, res, next) => {
    try {
        userService.contactUs(req, res, function (err, info) {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, info, "0", res.__('AddContactUs'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, info, "1", res.__('errorAddContactUs'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.addCustomerDetails = (req, res, next) => {
    try {
        userService.customerDetails(req, res, function (err, info) {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, info, "0", res.__('AddCustomerDetails'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, info, "1", res.__('errorAddCustomerDetails'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.addServiceRequest = (req, res, next) => {
    try {
        userService.serviceRequest(req, res, function (err, info) {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, info, "0", res.__('AddServiceRequest'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, info, "1", res.__('errorAddServiceRequest'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getContactUs = (req, res, next) => {
    try {
        userService.getContactUs(req, res, (err, contactUs) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, contactUs, "0", res.__('GetContactUs'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, contactUs, "1", res.__('errorGetContactUs'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getCustomerDetails = (req, res, next) => {
    try {
        userService.getCustomerDetails(req, res, (err, customerDetails) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, customerDetails, "0", res.__('GetCustomerDetails'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, customerDetails, "1", res.__('errorGetCustomerDetails'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getServiceRequests = (req, res, next) => {
    try {
        userService.getServiceRequest(req, res, (err, serviceRequest) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, serviceRequest, "0", res.__('GetServiceRequests'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, serviceRequest, "1", res.__('errorGetServiceRequests'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.changePassword = (req, res, next) => {
    try {
        userService.changePassword(req, res, (err, passwordChck) => {
            if(err || passwordChck == undefined){
                return sendResponse.sendJsonResponse(req, res, 201, passwordChck, "1", res.__('errorChangePassword'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 200, passwordChck, "0", res.__('ChangePassword'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteCustomerDetails = (req, res, next) => {
    try {
        userService.deleteCustomerDetails(req, res, (err, customerDetails) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, customerDetails, "0", res.__('DeleteCustomerDetails'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, customerDetails, "1", res.__('errorDeleteCustomerDetails'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteContactUs = (req, res, next) => {
    try {
        userService.deleteContactUs(req, res, (err, contactUs) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, contactUs, "0", res.__('DeleteContactUs'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, contactUs, "1", res.__('errorDeleteContactUs'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteServiceRequest = (req, res, next) => {
    try {
        userService.deleteServiceRequest(req, res, (err, serviceRequest) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, serviceRequest, "0", res.__('DeleteServiceRequest'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, serviceRequest, "1", res.__('errorDeleteServiceRequest'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.addLocalStores = (req, res, next) => {
    try {
        userService.addLocalStores(req, res, (err, localStores) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, localStores, "0", res.__('AddDealer'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, localStores, "1", res.__('errorAddDealer'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.updateLocalStores = (req, res, next) => {
    try {
        userService.updateLocalStores(req, res, (err, localStores) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, localStores, "0", res.__('UpdateDealer'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, localStores, "1", res.__('errorUpdateDealer'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteLocalStores = (req, res, next) => {
    try {
        userService.deleteLocalStores(req, res, (err, localStores) => {
            if(!err){
                    return sendResponse.sendJsonResponse(req, res, 200, localStores, "0", res.__('Deletedealer'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, localStores, "1", res.__('errorDeletedealer'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getLocalStores = (req, res, next) => {
    try {
        userService.getLocalStores(req, res, (err, localStores) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, localStores, "0", res.__('GetDealer'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, localStores, "1", res.__('errorGetDealer'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getAllLocalStores = (req, res, next) => {
    try {
        userService.getAllLocalStores(req, res, (err, localStores) => {
            if (!err) {
                    return sendResponse.sendJsonResponse(req, res, 200, localStores, "0", res.__('GetDealetList'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, localStores, "1", res.__('errorGetDealetList'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getImageCords = (req, res, next) => {
    try {
        userService.getImageCords(req, res, (err, imageCords) => {
           if(!err && imageCords.length !== 0){
                    return sendResponse.sendJsonResponse(req, res, 200, imageCords, "0", res.__('GetImageCords'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, imageCords, "1", res.__('errorGetImageCords'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getImageWebCords = (req, res, next) => {
    try {
        userService.getImageWebCords(req, res, (err, imageCords) => {
           if(!err && imageCords.length !== 0){
                    return sendResponse.sendJsonResponse(req, res, 200, imageCords, "0", res.__('GetWebImageCords'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, imageCords, "1", res.__('errorGetWebImageCords'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteImageCords = (req, res, next) => {
    try {
        userService.deleteImageCords(req, res, (err, cmsPage) => {
            if(!err){
                    return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('deleteImageCords'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errordeleteImageCords'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.addCmsPages = (req, res, next) => {
    try {
        userService.addCmsPages(req, res, (err, cmsPage) => {
            if(!err){
                    return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('AddCmsPages'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errorAddCmsPages'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.updateCmsPages = (req, res, next) => {
    try {
        userService.updateCmsPages(req, res, (err, cmsPage) => {
            if(!err){
                    return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('UpadateCmsPages'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errorUpadateCmsPages'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteCmsPages = (req, res, next) => {
    try {
        userService.deleteCmsPages(req, res, (err, cmsPage) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('DeleteCmsPages'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errorDeleteCmsPages'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getAllCmsPages = (req, res, next) => {
    try {
        userService.getAllCmsPages(req, res, (err, cmsPage) => {
                if(!err){
                    return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('GetAllCmsPages'));
                }else{
                    return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errorGetAllCmsPages'));
                }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getCmsPage = (req, res, next) => {
    try {
        userService.getCmsPage(req, res, (err, cmsPage) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, cmsPage, "0", res.__('GetCmsPage'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, cmsPage, "1", res.__('errorGetCmsPage'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.CustomerDetailsCsv = (req, res, next) => {
    try {
        userService.CustomerDetailsCsv(req, res, (err, customerDetails) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, customerDetails, "0", res.__('GetCustomerDetails'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, customerDetails, "1", res.__('errorGetCustomerDetails'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.ContactUsCsv = (req, res, next) => {
    try {
        userService.ContactUsCsv(req, res, (err, contactUs) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, contactUs, "0", res.__('GetContactUs'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, contactUs, "1", res.__('errorGetContactUs'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.ServiceRequestCsv = (req, res, next) => {
    try {
        userService.ServiceRequestCsv(req, res, (err, serviceRequest) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, serviceRequest, "0", res.__('GetServiceRequests'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, serviceRequest, "1", res.__('errorGetServiceRequests'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}
