const colorService = require('../../services/v1/color.service');
const sendResponse = require('../../modules/response.helper');
var i18n = require('./../../lanTan');

exports.addColor = (req, res, next) => {
    try {
        colorService.color(req, res, function (err, info) {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, info, "0", res.__('AddColor'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, info, "1", res.__('errorAddColor'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.addColorShade = (req, res, next) => {
    try {
        colorService.colorShade(req, res, function (err, info) {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, info, "0", res.__('AddColorShade'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, info, "1", res.__('errorAddColorShade'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getColors = (req, res, next) => {
    try {
        colorService.getColor(req, res, (err, color) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, color, "0", res.__('GetColors'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, color, "1", res.__('errorGetColors'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.getColorShades = (req, res, next) => {
    try {
        colorService.getColorShade(req, res, (err, colorShade) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, colorShade, "0", res.__('GetColorShade'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, colorShade, "1", res.__('errorGetColorShade'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.editColors = (req, res, next) => {
    try {
        colorService.editColor(req, res, (err, newColor) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, newColor, "0", res.__('EditColor'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, newColor, "1", res.__('errorEditColor'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.editColorShades = (req, res, next) => {
    try {
        colorService.editColorShade(req, res, (err, newColorShade) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, newColorShade, "0", res.__('EditColorShade'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, newColorShade, "1", res.__('errorEditColorShade'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteColor = (req, res, next) => {
    try {
        colorService.deleteColor(req, res, (err, color) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, color, "0", res.__('DeleteColor'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, color, "1", res.__('errorDeleteColor'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}

exports.deleteColorShade = (req, res, next) => {
    try {
        colorService.deleteColorShade(req, res, (err, colorShade) => {
            if(!err){
                return sendResponse.sendJsonResponse(req, res, 200, colorShade, "0", res.__('DeleteColorShade'));
            }else{
                return sendResponse.sendJsonResponse(req, res, 201, colorShade, "1", res.__('errorDeleteColorShade'));
            }
    });
} catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, "1", res.__('error'));
    }
}