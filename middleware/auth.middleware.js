// Load user model.
const Investor = require('../models/investors.model');
const Manager = require('../models/manager.model');
const sendResponse = require('../modules/response.helper');

module.exports.verifyToken = function (req, res, next) {

    var bearerToken;
    var bearerHeader = req.headers["accesstoken"];

    if (typeof bearerHeader !== 'undefined' && bearerHeader !== '') {

        var bearer = bearerHeader.split(" ");
        bearerToken = bearer[1];
        // verifies secret and checks exp

        if (!bearerToken)
            return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "No token provided.");
        Investor.findOne({'accessToken': bearerToken}, '-password').then(user => {

            if (user) {
                global.authenticationId = user;
                next();
            } else {
                global.authenticationId = "";
                return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "Unauthenticated");
            }
        })

    } else {
        return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "No token provided.");
    }

};

module.exports.verifyManagerToken = function (req, res, next) {

    var bearerToken;
    var bearerHeader = req.headers["accesstoken"];

    if (typeof bearerHeader !== 'undefined' && bearerHeader !== '') {

        var bearer = bearerHeader.split(" ");
        bearerToken = bearer[1];
        // verifies secret and checks exp

        if (!bearerToken)
            return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "No token provided.");
        Manager.findOne({'accessToken': bearerToken}, '-password').then(user => {

            if (user) {
                global.authenticationId = user;
                next();
            } else {
                global.authenticationId = "";
                return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "Unauthenticated");
            }
        })

    } else {
        return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "No token provided.");
    }

};

