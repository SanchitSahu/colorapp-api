const contactUsModel = require('../../models/contact_us.model');
const customerDetailsModel = require('../../models/customer_details.model');
const serviceRequestModel = require('../../models/service_request.model');
const adminModel = require('../../models/admin.model');
const colorModel = require('../../models/color.model');
const colorShadeModel = require('../../models/color_shade.model');
const cmsPagesModel = require('../../models/cmsPages.model');
const localStoresModel = require('../../models/local_store.model');
const bcrypt = require('bcryptjs');
const async = require('async');
const imageCordsModel = require('../../models/image_cords.model');
const imageWebCordsModel = require('../../models/imageWeb_cords.model');
const json2csv = require('json2csv').parse;
const fs = require('fs');
var csv = require('csv-express');


exports.signIn = async (adminData) => {
    try {
        var checkedAdmin = await adminModel.findOne({ 'username': adminData.username });
        return checkedAdmin;
    } catch (error) {

        
        throw Error('Error In Getting Admin Data');
    }
}

exports.getPassword = async (adminData) => {
    try {
        var checkedAdmin = await adminModel.findOne({ 'username': adminData.username }, 'password');
        return checkedAdmin;
    } catch (error) {
        throw Error('Error In getting data');
    }
}

exports.checkSignIn = async (username, password, pass) => {
    if (username != null && pass != null) {
        var validPassword = bcrypt.compareSync(pass, password);
        if (validPassword) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

exports.changePassword = async (req, res, callback) => {
    var oldPassword = req.body.oldPassword, newPassword = req.body.newPassword, confirmPassword = req.body.confirmPassword;
    adminModel.findOne().exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            if (bcrypt.compareSync(oldPassword, result.password)) {
                if (newPassword && confirmPassword) {
                    if (newPassword === confirmPassword) {
                        var hashedPassword = bcrypt.hashSync(newPassword, 6);
                        adminModel.updateOne({ $set: { password: hashedPassword } }, (err, data) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, data);
                            }
                        })
                    } else {
                        callback(err);
                    }
                } else {
                    callback(err);
                }
            } else {
                callback(err);
            }
        }
    })
}

exports.getDashboard = (callback) => {
    try {
        async.parallel([
            function (colorsCallback) {
                colorModel.count().exec((err, colors) => {
                    if (err) {
                        return err;
                    } else {
                        var totalColors = colors;
                        colorsCallback(null, totalColors);
                    }
                })
            },
            function (colorShadesCallback) {
                colorShadeModel.count().exec((err, colorShades) => {
                    if (err) {
                        return err;
                    } else {
                        var totalColorShades = colorShades;
                        colorShadesCallback(null, totalColorShades);
                    }
                })
            },
            function (contactUsCallback) {
                contactUsModel.count().exec((err, contactUs) => {
                    if (err) {
                        return err;
                    } else {
                        var totaclContactUs = contactUs;
                        contactUsCallback(null, totaclContactUs);
                    }
                })
            },
            function (customerDetailCallback) {
                customerDetailsModel.count().exec((err, customerDetails) => {
                    if (err) {
                        return err;
                    } else {
                        var totalCustomerDetails = customerDetails;
                        customerDetailCallback(null, totalCustomerDetails);
                    }
                })
            },
            function (serviceRequestCallback) {
                serviceRequestModel.count().exec((err, serviceRquests) => {
                    if (err) {
                        return err;
                    } else {
                        var totalServiceRequests = serviceRquests;
                        serviceRequestCallback(null, totalServiceRequests);
                    }
                })
            },
            function (cmsPagesCallback) {
                cmsPagesModel.count().exec((err, cmsPages) => {
                    if (err) {
                        return err;
                    } else {
                        var totalcmsPages = cmsPages;
                        cmsPagesCallback(null, totalcmsPages);
                    }
                })
            },
            function (imageCordsCallback) {
                imageCordsModel.count().exec((err, imageCords) => {
                    if (err) {
                        return err;
                    } else {
                        var totalimageCords = imageCords;
                        imageCordsCallback(null, totalimageCords);
                    }
                })
            }
        ], function (error, result) {
            var color = 0, colorShade = 0, contactUs = 0, customerDetails = 0, serviceRequest = 0, cmsPages = 0, imageCords = 0;
            color = result[0];
            colorShade = result[1];
            contactUs = result[2];
            customerDetails = result[3];
            serviceRequest = result[4];
            cmsPages = result[5];
            imageCords = result[6];

            var finalResult = {
                color: color,
                colorShade: colorShade,
                contactUs: contactUs,
                customerDetails: customerDetails,
                serviceRequest: serviceRequest,
                cmsPages: cmsPages,
                imageCords: imageCords
            };
            callback(finalResult);
        })
    } catch (error) {
        throw Error('Error In Gettong Dashboard');
    }
}

exports.contactUs = (req, res, callback) => {
    var contactUs = {
        name: req.body.name,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        message: req.body.message,
        city: req.body.city
    }
    contactUsModel(contactUs).save((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.customerDetails = (req, res, callback) => {

    var dt = typeof (req.body.colorId);
    if (dt == "string") {
        var cid = req.body.colorId.replace("'", "").split(',');
        for (var i = 0; i < cid.length; i++) {
            cid[i] = (cid[i]).toString();
        }
    } else {
        var cid = req.body.colorId;
    }

    var customerDetails = {
        name: req.body.name,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        zipcode: req.body.zipcode,
        city: req.body.city,
        colorId: cid
    }
    customerDetailsModel(customerDetails).save((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.serviceRequest = (req, res, callback) => {
    var serviceRequest = {
        name: req.body.name,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        zipcode: req.body.zipcode,
        requestType: req.body.requestType,
        city: req.body.city
    }
    serviceRequestModel(serviceRequest).save((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getContactUs = (req, res, callback) => {
    contactUsModel.find().exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getCustomerDetails = (req, res, callback) => {
    customerDetailsModel.find().populate("colorId").exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getServiceRequest = (req, res, callback) => {
    serviceRequestModel.find().exec((err, result) => {
        if (err) {
            callback(err, null)
        } else {
            callback(null, result);
        }
    })
}

exports.deleteCustomerDetails = (req, res, callback) => {
    customerDetailsModel.findOneAndRemove({ _id: req.body.id }, (err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.deleteContactUs = (req, res, callback) => {
    contactUsModel.findOneAndRemove({ _id: req.body.id }, (err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.deleteServiceRequest = (req, res, callback) => {
    serviceRequestModel
        .findOneAndRemove({ _id: req.body.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
}

exports.addLocalStores = (req, res, callback) => {
    var localStoresDetails = {
        name: req.body.name,
        address: req.body.address,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        city: req.body.city,
        phoneNumber: req.body.phoneNumber
    }
    localStoresModel(localStoresDetails).save((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.updateLocalStores = (req, res, callback) => {
    localStoresModel.findOne({ _id: req.body.id }, (err, result) => {
        if (err) {
            callback(err, null);
        } else {
            let name = result.name;
            let address = result.address;
            let latitude = result.latitude;
            let longitude = result.longitude;
            let city = result.city;
            let phoneNumber = result.phoneNumber;

            if (req.body.name) {
                name = req.body.name;
            }

            if (req.body.address) {
                address = req.body.address;
            }

            if (req.body.latitude) {
                latitude = req.body.latitude;
            }

            if (req.body.longitude) {
                longitude = req.body.longitude;
            }

            if (req.body.city) {
                city = req.body.city;
            }

            if (req.body.phoneNumber) {
                phoneNumber = req.body.phoneNumber;
            }

            localStoresModel.findOneAndUpdate({ _id: req.body.id }, { $set: { name: name, address: address, longitude: longitude, latitude: latitude, city: city, phoneNumber: phoneNumber } },
                { new: true }, (err, resultUpdated) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, resultUpdated);
                    }
                })
        }
    })
}

exports.deleteLocalStores = (req, res, callback) => {
    localStoresModel
        .findOneAndRemove({ _id: req.body.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
}

exports.getLocalStores = (req, res, callback) => {
    localStoresModel
        .findOne({ _id: req.params.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
}

exports.getAllLocalStores = (req, res, callback) => {
    localStoresModel.find().exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getImageCords = (req, res, callback) => {
    if (req.headers.coords) {
        var obj = {

            imagePath: "http://13.127.108.174:5000/images/00.jpg",

            coordinates: JSON.parse(req.headers.coords)
        }
        imageCordsModel(obj).save((err, resultSave) => {
            if (err) {
                callback(err, null);
            } else {
                imageCordsModel.find().exec((err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, result);
                    }
                })
            }
        })
    } else {
        imageCordsModel.find().exec((err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
    }
}

exports.getImageWebCords = (req, res, callback) => {
    if (req.headers.coords) {
        var obj = {

            imagePath: "http://13.127.108.174:5000/images/03.jpg",

            coordinates: JSON.parse(req.headers.coords)
        }
        imageWebCordsModel(obj).save((err, resultSave) => {
            if (err) {
                callback(err, null);
            } else {
                imageWebCordsModel.find().exec((err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, result);
                    }
                })
            }
        })
    } else {
        imageWebCordsModel.find().exec((err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
    }
}

exports.deleteImageCords = (req, res, callback) => {
    imageCordsModel
        .findOneAndRemove({ _id: req.body.id }, (err, result) => {
            if (err) {
                console.log("error", err);
                callback(err, null);
            } else {
                console.log("error", result);
                callback(null, result);
            }
        })
}

exports.addCmsPages = (req, res, callback) => {
    var cmsPages = {
        title: req.body.title,
        details: req.body.details,
        type: req.body.type,
        language: req.body.language,
    }
    cmsPagesModel(cmsPages).save((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.updateCmsPages = (req, res, callback) => {
    cmsPagesModel.findOne({ _id: req.body.id }, (err, result) => {
        if (err) {
            callback(err, null);
        } else {
            let title = result.title;
            let details = result.details;
            let type = result.type;
            let language = result.language;

            if (req.body.title) {
                title = req.body.title;
            }

            if (req.body.details) {
                details = req.body.details;
            }

            if (req.body.type) {
                type = req.body.type;
            }

            if (req.body.language) {
                launguage = req.body.language;
            }

            cmsPagesModel.findOneAndUpdate({ _id: req.body.id }, { $set: { title: title, details: details, type: type, language: language } },
                { new: true }, (err, resultUpdated) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, resultUpdated);
                    }
                })
        }
    })
}

exports.deleteCmsPages = (req, res, callback) => {
    cmsPagesModel
        .findOneAndRemove({ _id: req.body.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
}

exports.getAllCmsPages = (req, res, callback) => {
    cmsPagesModel.find().exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getCmsPage = (req, res, callback) => {
    let language = { language: req.params.language };
    let type = { type: req.params.type }
    let id = { _id: req.params.id }

    if (req.params.type && req.params.language) {
        cmsPagesModel.findOne({ $and: [language, type] }).exec((err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
    }

    if (req.params.id) {
        cmsPagesModel.findOne(id).exec((err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
    }

}

exports.CustomerDetailsCsv = (req, res, callback) => {
    customerDetailsModel.find().populate("colorId").exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            var arrDetails = []
            for (var i = 0; i < result.length; i++) {
                var id = result[i].colorId;
                var name;
                let ColorName = []
                id.forEach(el => {
                    ColorName.push(el.colorName);
                });
                let objDetails = {
                    Email: result[i].email,
                    Name: result[i].name,
                    PhoneNumber: result[i].phoneNumber,
                    ColorName
                }
                arrDetails.push(objDetails)
            }
            var csv = json2csv(arrDetails);
            res.setHeader('Content-disposition', 'attachment; filename=customerDetails.csv');
            res.set('Content-Type', 'text/csv');
            res.status(200).send(csv);
        }
    })
}

exports.ContactUsCsv = (req, res, callback) => {
    contactUsModel.find().exec((err, result) => {
        if (err) {
            callback(err, null);
        } else {
            var arrDetails = []
            for (var i = 0; i < result.length; i++) {
                let objDetails = {
                    Email: result[i].email,
                    Name: result[i].name,
                    PhoneNumber: result[i].phoneNumber,
                    Massage:  result[i].message
                }
                arrDetails.push(objDetails)
            }
            var csv = json2csv(arrDetails);
            res.setHeader('Content-disposition', 'attachment; filename=contactUs.csv');
            res.set('Content-Type', 'text/csv');
            res.status(200).send(csv);
        }
    })
}

exports.ServiceRequestCsv = (req, res, callback) => {
    serviceRequestModel.find().exec((err, result) => {
        if (err) {
            callback(err, null)
        } else {
            var arrDetails = []
            for (var i = 0; i < result.length; i++) {
                let objDetails = {
                    Name: result[i].name,
                    Email: result[i].email,
                    PhoneNumber: result[i].phoneNumber,
                    RequestType:  result[i].requestType
                }
                arrDetails.push(objDetails)
            }
            var csv = json2csv(arrDetails);
            res.setHeader('Content-disposition', 'attachment; filename=serviceRequest.csv');
            res.set('Content-Type', 'text/csv');
            res.status(200).send(csv);
        }
    })
}

