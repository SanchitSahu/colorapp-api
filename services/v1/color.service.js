const colorModel = require('../../models/color.model');
const colorShadeModel = require('../../models/color_shade.model');
const hexToHsl = require('hex-to-hsl');
const sortBy = require('sort-by');

exports.color = (req, res, callback) => {
    var hsl = hexToHsl(req.body.colorCode);
    var lightness = hsl[2];
    var color = {
        name: req.body.name,
        colorCode: req.body.colorCode,
        lightness: lightness
    }
    colorModel(color).save((err, result) => {
        if (err || result == undefined) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.colorShade = (req, res, callback) => {
    var hsl = hexToHsl(req.body.colorCode);
    var lightness = hsl[2];
    var colorShade = {
        colorId: req.body.colorId,
        shadeName: req.body.shadeName,
        colorCode: req.body.colorCode,
        lightness: lightness,
        colorName: req.body.colorName
    }
    colorShadeModel(colorShade).save((err, result) => {
        if (err || result == undefined) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    })
}

exports.getColor = (req, res, callback) => {
    let colId = req.params.colorId;
    if (!colId || colId == undefined) {
        colorModel.find().exec((err, result) => {
            if (err) {
                callback(err, null)
            } else {
                result.sort(sortBy('colorId', 'lightness')).reverse();
                callback(null, result);
            }
        })
    } else {
        colorModel.find({ _id: colId }).exec((err, result) => {
            if (err) {
                callback(err, null)
            } else {

                callback(null, result);
            }
        })
    }
}


exports.getColorShade = (req, res, callback) => {
    var colId = req.params.colorId;
    var colShadeId = req.params.colorShadeId;
    var single = req.params.single;
    if (!colId || colId == undefined) {
        if (!colShadeId || colShadeId == undefined) {
            colorShadeModel.find().populate('colorId').exec((err, result) => {
                if (err) {
                    callback(err, null)
                } else {
                    result.sort(sortBy('lightness')).reverse();
                    callback(null, result);

                }
            })
        }
    }
    if (colId) {
        colorShadeModel.find({ colorId: colId }).exec((err, result) => {
            if (err) {
                callback(err, null)
            } else {
                result.sort(sortBy('lightness')).reverse();
                callback(null, result);
            }
        })
    }

    if (colShadeId) {
        if (single) {
            if (colId == undefined || !colId) {
                colorShadeModel.find({ _id: colShadeId }).exec((err, result) => {
                    if (err) {
                        callback(err, null)
                    } else {
                        result.sort(sortBy('lightness')).reverse();
                        callback(null, result);
                    }
                })
            }
        }
    }
}



exports.editColor = (req, res, callback) => {
    colorModel.findOne({ _id: req.body.id }, function (err, resultFind) {
        if (err) {
            callback(err, null);
        } else {

            let name = resultFind.name;
            let colorCode = resultFind.colorCode;

            if (req.body.name) {
                name = req.body.name;
            }

            if (req.body.colorCode) {
                colorCode = req.body.colorCode;
            }
            var hsl = hexToHsl(colorCode);
            var lightness = hsl[2];

            if (req.body.id != null) {
                colorModel.findOneAndUpdate({ _id: req.body.id },
                    {
                        $set: {
                            name: name,
                            colorCode: colorCode,
                            lightness: lightness
                        }
                    },
                    (err, result) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, result);
                        }
                    })
            } else {
                callback(err, err);
            }
        }
    })
}

exports.editColorShade = (req, res, callback) => {
    colorShadeModel.findOne({ _id: req.body.id }, function (err, resultFind) {
        if (err) {
            callback(err, null);
        } else {

            let shadeName = resultFind.shadeName;
            let colorCode = resultFind.colorCode;
            let colorId = resultFind.colorId;

            if (req.body.shadeName) {
                shadeName = req.body.shadeName;
            }

            if (req.body.colorCode) {
                colorCode = req.body.colorCode;
            }

            if (req.body.colorId) {
                colorId = req.body.colorId;
            }
            var hsl = hexToHsl(colorCode);
            var lightness = hsl[2];

            colorShadeModel.findOneAndUpdate({ _id: req.body.id },
                {
                    $set: {
                        colorId: colorId,
                        shadeName: shadeName,
                        colorCode: colorCode,
                        lightness: lightness
                    }
                },
                (err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, result);
                    }
                })
        }
    })
}

exports.deleteColor = (req, res, callback) => {
    if (req.body.id != null) {
        colorModel.findOne({ _id: req.body.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                if (result != null) {
                    colorShadeModel.deleteMany({ colorId: result._id }, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            colorModel.deleteOne({ _id: result._id }, (err, colorData) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, result);
                                }
                            })
                        }
                    })
                } else {
                    callback(err, null);
                }
            }
        })
    } else {
        callback(err, null);
    }

}

exports.deleteColorShade = (req, res, callback) => {
    if (req.body.id != null) {
        colorShadeModel.findOneAndRemove({ _id: req.body.id }, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        })
    } else {
        callback(err, null);
    }

}