var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var imageCordsSchema = new mongoose.Schema({
    imagePath: String,
    coordinates: Object
}, {
        collection: 'image_cords'
    });
module.exports = mongoose.model('image_cords', imageCordsSchema);