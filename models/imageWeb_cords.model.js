var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var imageWebCordsSchema = new mongoose.Schema({
    imagePath: String,
    coordinates: Object
}, {
        collection: 'imageWeb_cords'
    });
module.exports = mongoose.model('imageWeb_cords', imageWebCordsSchema);