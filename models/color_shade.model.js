var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var unquieValidator = require('mongoose-unique-validator');

var colorShadeSchema = new mongoose.Schema({
    colorId: { type: Schema.Types.ObjectId, ref: 'color' },
    shadeName: { type: String, unique: true, required: true },
    colorCode: { type: String, required: true , unique: true},
    lightness: { type: String, required: true },
    colorName: { type: String, required: true },
}, {
        collection: 'color_shade'
    });
colorShadeSchema.plugin(unquieValidator);
module.exports = mongoose.model('color_shade', colorShadeSchema);