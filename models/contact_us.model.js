var mongoose = require('mongoose');

var contactUs = new mongoose.Schema({
    name:String,
    email:String,
    phoneNumber:String,
    message:String,
    city: String
}, {
    collection: 'contactUs'
});
module.exports = mongoose.model('contactUs', contactUs);