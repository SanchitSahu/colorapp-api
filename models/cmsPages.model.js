var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var cmsPageSchema = new mongoose.Schema({
    title: {type: String, required: true},
    details: {type: String, required: true},
    type: {type: String, required: true},
    language: {type: String, required: true},
}, {
        collection: 'cms_pages'
    });
module.exports = mongoose.model('cms_pages', cmsPageSchema);