var mongoose = require('mongoose');

var customerDetailsSchema = new mongoose.Schema({
    name: String,
    email: String,
    phoneNumber:String,
    zipcode:String,
    city: String,
    colorId:[{type: mongoose.Schema.Types.ObjectId, ref: 'color_shade'}]
}, {
    collection: 'customer_details'
});
module.exports = mongoose.model('customer_details', customerDetailsSchema);	
