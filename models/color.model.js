var mongoose = require('mongoose');
var uniqueValidator=require('mongoose-unique-validator');

var colorSchema = new mongoose.Schema({
    name: {type:String,unique:true,required:true},
    colorCode:{type:String,required:true},
    lightness:{type:String,required:true}
}, {
    collection: 'color'
});
colorSchema.plugin(uniqueValidator);
module.exports = mongoose.model('color', colorSchema);