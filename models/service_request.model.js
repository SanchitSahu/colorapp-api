var mongoose = require('mongoose');

var serviceRequestSchema = new mongoose.Schema({
    name: String,
    email: String,
    phoneNumber:String,
    zipcode:String,
    requestType:String,
    city: String
}, {
    collection: 'service_request'
});
module.exports = mongoose.model('service_request', serviceRequestSchema);