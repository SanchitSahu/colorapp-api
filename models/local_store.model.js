var mongoose = require('mongoose');

var localStoresSchema = new mongoose.Schema({
    name: {type:String,required:true},
    address: {type:String,required:true},
    latitude: {type:String,required:true},
    longitude: {type:String,required:true},
    city: {type:String,required:true},
    phoneNumber:{type:String,required:true,minlength:'10'},
}, {
    collection: 'locate_dealer'
});
module.exports = mongoose.model('locate_dealer', localStoresSchema);